import datetime
import json
import os
import random
import time
from io import StringIO

import boto3
import dateutil
import pandas as pd
import pytz
from _utils.read_file import read_json_local
from _utils.save_file import save_json_local

DIR_DATE = datetime.datetime.now().strftime("%Y%m%d%H%M%S")


class AWSWorker:
    def __init__(self, service_name):
        credentials_path = os.path.join(
            os.getcwd(), "credentials", "credentials_aws.json"
        )
        if os.path.exists(credentials_path):
            credentials = read_json_local(credentials_path)
        else:
            raise ValueError("Something wrong with the credentials")

        self.duration_seconds = credentials.get("duration_seconds")
        self.serial_number = credentials.get("serial_number")
        self.token_path = credentials.get("token_path")
        self.database = credentials.get("database")
        self.catalog = credentials.get("catalog")
        self.bucket = credentials.get("bucket")
        self.bucket_queries = credentials.get("bucket_queries")
        self.queries_path = credentials.get("queries_path")
        self.connection = self.connect(service_name=service_name)
        self.backedup_s3 = False
        self.file_partition = 0

    def mfa_session(self, token_path, duration_seconds, serial_number):
        session = boto3.session.Session()
        sts_connection = session.client("sts")
        if os.path.exists(token_path):
            session = read_json_local(token_path)
            utc = pytz.UTC
            mfa_expire = dateutil.parser.parse(session["Credentials"]["Expiration"])
            time_now = datetime.datetime.today().replace(tzinfo=utc)

            if time_now > mfa_expire:
                mfa_code = input("Enter the MFA code: ")
                session = sts_connection.get_session_token(
                    DurationSeconds=duration_seconds,
                    SerialNumber=serial_number,
                    TokenCode=mfa_code,
                )
                save_json_local(data=session, path=token_path)
        else:
            mfa_code = input("Enter the MFA code: ")
            session = sts_connection.get_session_token(
                DurationSeconds=duration_seconds,
                SerialNumber=serial_number,
                TokenCode=mfa_code,
            )
            save_json_local(data=session, path=token_path)
        return session

    def connect(self, service_name):
        session = self.mfa_session(
            self.token_path, self.duration_seconds, self.serial_number
        )
        connection = boto3.client(
            region_name="eu-west-1",
            service_name=service_name,
            aws_access_key_id=session["Credentials"]["AccessKeyId"],
            aws_secret_access_key=session["Credentials"]["SecretAccessKey"],
            aws_session_token=session["Credentials"]["SessionToken"],
        )
        return connection

    def athena_queries(self, query):
        response = self.connection.start_query_execution(
            QueryString=query,
            ClientRequestToken=str(round(random.random() * (10 ** 64))),
            QueryExecutionContext={"Database": self.database, "Catalog": self.catalog},
            ResultConfiguration={"OutputLocation": self.bucket_queries},
            WorkGroup="xd-ehrd-production",
        )
        return response
    
    def query_to_df(self, query):
        response = self.athena_queries(query)

        query_status = None
        while query_status == 'QUEUED' or query_status == 'RUNNING' or query_status is None:
            query_status = self.connection.get_query_execution(QueryExecutionId=response["QueryExecutionId"])['QueryExecution']['Status']['State']
            print(query_status)
            if query_status == 'FAILED' or query_status == 'CANCELLED':
                raise Exception('Athena query with the string failed or was cancelled')
            time.sleep(5)
        print('Query finished')
        aws_s3 = AWSWorker("s3")
        athena_query_key = "{0}{1}.csv".format(self.queries_path, response["QueryExecutionId"])
        csv_string = aws_s3.connection.get_object(Bucket=self.bucket, Key=athena_query_key)["Body"].read().decode("utf-8")
        df = pd.read_csv(StringIO(csv_string))
        return df
    
    def athena_sample(self, response):
        results = self.connection.get_query_results(QueryExecutionId=response["QueryExecutionId"], MaxResults=10)
        df = pd.DataFrame([[data.get('VarCharValue') for data in row['Data']] for row in results['ResultSet']['Rows']])
        return df
    
    def s3_prepare_apparitions(self, bucket_name, project_prefix, backup_prefix):

        # Creating a backup of apparitions
        s3_objects = self.connection.list_objects_v2(Bucket=bucket_name, Prefix=project_prefix)
        if s3_objects.get("Contents") != None: 
            self._backup_objects(s3_objects_source=s3_objects["Contents"], bucket_source=bucket_name, backup_prefix=backup_prefix)
            # Deleting existing files to add new ones
            to_delete = [{"Key": obj["Key"]} for obj in s3_objects["Contents"]] # Maybe it is needed to drop `project_prefix`
            self.connection.delete_objects(Bucket=bucket_name, Delete={"Objects": to_delete})
        self.backedup_s3 = True
        print("Successful backup at {0}".format(backup_prefix))
    
    def s3_store_df(self, bucket_name, prefix, project_name, df):
        project_prefix = os.path.join(prefix, project_name, "master", "")
        backup_prefix = os.path.join(prefix, project_name, "backup", "")
        if self.backedup_s3 == False:
            self.s3_prepare_apparitions(bucket_name, project_prefix, backup_prefix)
        project_prefix_part = os.path.join(project_prefix, "apparitions_part_{0}.csv".format(self.file_partition))
        csv_buffer = StringIO()
        df.to_csv(csv_buffer, sep="|")
        self.connection.put_object(Bucket=bucket_name, Key=project_prefix_part,Body=csv_buffer.getvalue())
        self.file_partition += 1
    
    def _backup_objects(self, s3_objects_source, bucket_source, backup_prefix):
        backup_prefix = os.path.join(backup_prefix, "backup_{0}".format(DIR_DATE), "")
        copy_sources = [{"Bucket": bucket_source, "Key": obj["Key"]} for obj in s3_objects_source]
        for copy_source in copy_sources:
            destiny_key = os.path.join(backup_prefix, copy_source["Key"].split("/")[-1])
            self.connection.copy(copy_source, bucket_source, destiny_key)
    
    
    

    


if __name__ == "__main__":
    aws = AWSWorker("athena")
    bucket = "ehread-maestro-tables"
    aws.athena_queries

