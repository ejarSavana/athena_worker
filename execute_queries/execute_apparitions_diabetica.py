from table_scripts.apparitions_to_data import apparitions_to_data

filters = """
    WHERE age >= 18
    AND age <= 120
    AND index_date >= date('2013-01-01')
    AND index_date <= date('2018-12-31')
"""
apparitions_to_data(database="default", project="diabetica", filters=filters)