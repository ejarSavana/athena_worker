import json
import time

import pandas as pd
from connections.AWSClient import AWSWorker
from table_scripts.followup_table import (followup_info_dict,
                                          followup_n_info_dict, followup_query)

aws = AWSWorker("athena")

project_view_name = "v_cleanup_diabetica"
project_name = "diabetica"
period_stride_months = 24
window_months_before = 6
window_months_after = 6
database="default"
filters = """
    WHERE age >= 18
    AND age <= 120
    AND index_date >= date('2013-01-01')
    AND index_date <= date('2018-12-31')
"""

followup_times = 0
all_dfs = list()
while True:

   

    query, followup_table_name = followup_query(
        project_view_name=project_view_name,
        project_name=project_name,
        period_stride_months=period_stride_months*followup_times,
        window_months_before=window_months_before,
        window_months_after=window_months_after,
        database=database,
    )

    response = aws.athena_queries(query)
    query_dict = followup_info_dict(project_name=project_name, followup_table=followup_table_name, filters=filters)
    time.sleep(5)
    df = aws.query_to_df(query_dict)
    df["patient_info"] = df["patient_info"].apply(lambda x: json.loads(x))
    patient_info = dict()
    
    for _, patient_id, dict_col in df.itertuples():
        patient_info.update({patient_id: dict_col})
    new_df = pd.DataFrame(patient_info).T
    new_df = new_df.add_suffix("_{0}MONTHS".format(period_stride_months*followup_times))
    all_dfs.append(new_df)

    followup_times += 1
    print(len(all_dfs)-1)
    print(new_df.shape)
    if new_df.shape[0] == 0:
        break
final_df = pd.concat(all_dfs, axis=1, sort=False)

