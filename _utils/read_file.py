import json

def read_json_local(path):
    with open(path) as f:
        data = json.load(f)
    return data
