import json

# TODO(ejar): Path as arguments
def save_json_local(data, path):
    with open(path, 'w') as f:
        json.dump(data, f, sort_keys=True, default=str)