from connections.AWSClient import AWSWorker
import time


def apparitions_to_data(project):
    aws = AWSWorker("athena")
    query = """
    SELECT *
    FROM "default"."cleanup_{project}"
    LIMIT 10
    """.format(
        project=project
    )
    response = aws.athena_queries(query)
    time.sleep(5)
    df = aws.athena_sample(response=response)
    print(df)



apparitions_to_data("page")