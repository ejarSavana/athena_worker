from connections.AWSClient import AWSWorker


def apparitions_to_data(database, project, filters):
    aws = AWSWorker("athena")

    query = """
    CREATE OR REPLACE VIEW {database}.v_cleanup_{project} AS
    SELECT *
    FROM (
        SELECT 
            database_name,
            var_name,
            study_vars_type,
            patient_id,
            term_id,
            record_id,
            param,
            DATE(metadata_date) AS metadata_date,
            result_str,
            results_col,
            reportdate,
            hospital_id,
            patient_client_id,
            birth,
            DATE(birthdate) AS birthdate,
            timestamp,
            service,
            report_type,
            doctor,
            admission_days,
            revisits,
            readmissions,
            readmissions_24,
            readmissions_48,
            readmissions_72,
            exitus,
            emergency_admission,
            no_treatment,
            episode_id,
            admission_days_raw,
            concepts_acronymized,
            acronyms_file,
            from_acronym,
            acronyms,
            acronyms_expansions,
            breakfast,
            breakfast_2h,
            breakfast_4h,
            breakfast_6h,
            lunch,
            lunch_10h,
            lunch_12h,
            lunch_14h,
            dinner,
            dinner_18h,
            dinner_20h,
            dinner_22h,
            description,
            missing_dose,
            tapering,
            effectiveness,
            period,
            period_type,
            terminology,
            supplier_name,
            test_id,
            result,
            unit_type,
            medtest,
            doctor_name,
            CAST(sex AS integer) AS sex,
            CAST(type AS integer) AS type,
            CAST(age AS integer) AS age,
            CAST(negation AS integer) AS negation,
            DATE(index_date) AS index_date,
            CAST(json_extract(replace(substring(metadata, 2, length(metadata)-1), '""', '"'), '$.dose')AS array(varchar)) as dose,
            metadata
            FROM "default"."cleanup_{project}" 
            )
        {filters}
    """.format(
        database=database, project=project, filters=filters
    )
    aws.athena_queries(query)


