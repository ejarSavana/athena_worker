from connections.AWSClient import AWSWorker
import time
import pandas as pd
import json

def followup_query(
    project_view_name,
    project_name,
    period_stride_months,
    window_months_before,
    window_months_after,
    database="default",
):
    adapted_data_query = """
        SELECT IF(
                (study_vars_type = 'param'),
                concat(concat(var_name, '_'), medtest),
                var_name
            ) AS var_name,
            patient_id,
            record_id,
            study_vars_type,
            CAST(sex AS integer) AS sex,
            CAST(type AS integer) AS type,
            CAST(age AS integer) AS age,
            CAST(negation AS integer) AS negation,
            date(index_date) AS index_date,
            results_col
        FROM {database}.{project_view_name}
    """.format(
        project_view_name=project_view_name, database=database
    )

    # adapted_age_as_var_query = """
    #     SELECT patient_id,
    #         var_name,
    #         results_col,
    #         study_vars_type,
    #         type,
    #         negation,
    #         index_date
    #     FROM {database}.{project_view_name}
    #     UNION ALL
    #     SELECT patient_id,
    #         'age' AS var_name,
    #         CAST(min(age) AS varchar) AS results_col,
    #         'param' AS study_vars_type,
    #         8 AS type,
    #         0 AS negation,
    #         CAST(min(index_date) AS date) AS index_date
    #     FROM ({database}.{adapted_data_query})
    #     GROUP BY patient_id
    # """.format(
    #     adapted_data_query=adapted_data_query,
    #     project_view=project_view_name,
    #     database=database,
    # )

    pre_followup_table = """
        WITH followup_t AS (
            SELECT patient_id,
                (min(index_date) + INTERVAL '{period_stride_months}' MONTH) AS followup,
                min(index_date) AS min_index_date_patient
            FROM ({adapted_data_query}) AS tmp_followup
            GROUP BY "patient_id"
        )
        SELECT followup_t.followup,
            followup_t.min_index_date_patient,
            abs(
                date_diff(
                    'day',
                    followup_t.followup,
                    tmp_followup.index_date
                )
            ) AS diff_days,
            tmp_followup.*
        FROM (
                ({adapted_data_query}) AS tmp_followup
                INNER JOIN followup_t ON (
                    followup_t.patient_id = tmp_followup.patient_id
                )
            )
        WHERE (
                (
                    index_date <= (followup + INTERVAL '{window_months_before}' MONTH)
                )
                AND (
                    index_date >= (followup - INTERVAL '{window_months_after}' MONTH)
                )
            )
        """.format(
        database=database,
        adapted_data_query=adapted_data_query,
        period_stride_months=period_stride_months,
        window_months_before=window_months_before,
        window_months_after=window_months_after,
    )
    
    # followup_dictionary = """
    # CREATE OR REPLACE VIEW v_followup_diabetica_{period_stride_months}_months AS 
    # SELECT t.patient_id,
    #     map_concat(map_agg(
    #         t.var_name,
    #         tmp_dict_table.results_col
    #     ), map(ARRAY['age', 'sex'], ARRAY[CAST (min(age) AS varchar), CAST (min(sex) AS varchar)])) AS patient_info
    # FROM ({followup_table}) AS tmp_dict_table
    #     join (
    #         SELECT min(diff_days) as min_diff_days,
    #             patient_id,
    #             var_name
    #         FROM ({followup_table}) AS tmp_dict_table
    #         group by patient_id,
    #             var_name
    #     ) AS t ON tmp_dict_table.patient_id = t.patient_id
    #     AND tmp_dict_table.var_name = t.var_name
    #     AND min_diff_days = diff_days) AS final_followup
    # group by t.patient_id
    # """.format(database=database, followup_table=followup_table)

    followup_table = """
    CREATE OR REPLACE VIEW {database}.v_followup_{project_name} AS 
    SELECT  t.patient_id AS patient_id
    , followup
    , min_index_date_patient
    , diff_days
    , min_diff_days
    , t.var_name AS var_name
    , record_id
    , sex
    , age
    , type
    , negation
    , index_date
    , results_col
    FROM ({pre_followup_table}) AS tmp_dict_table
        join (
            SELECT min(diff_days) as min_diff_days,
                patient_id,
                var_name
            FROM ({pre_followup_table}) AS tmp_dict_table
            group by patient_id,
                var_name
        ) AS t ON tmp_dict_table.patient_id = t.patient_id
        AND tmp_dict_table.var_name = t.var_name
        AND min_diff_days = diff_days
    """.format(database=database, pre_followup_table=pre_followup_table, project_name=project_name)
    
    query = followup_table
    followup_table_name = "{database}.v_followup_{project_name}".format(database=database, project_name=project_name)

    return query, followup_table_name

def followup_n_info_dict(project_name, followup_table, followup_info_dict, filters=""):
    
    followup_dictionary = """
    SELECT patient_id, CAST(map_agg(var_name, n_occurences) AS JSON) as n_patient_info 
    FROM (
    SELECT patient_id, var_name, count(*) as n_occurences
    FROM ({followup_table})
    {filters}
    GROUP BY patient_id, var_name
    ) GROUP BY patient_id
    """.format(followup_table=followup_table, project_name=project_name, filters=filters)
    return followup_dictionary


def followup_info_dict(project_name, followup_table, filters=""):

    followup_dictionary = """
    SELECT patient_id,
        CAST(map_concat(map_agg(
            var_name,
            results_col
        ), map(ARRAY['age', 'sex'], ARRAY[CAST (min(age) AS varchar), CAST (min(sex) AS varchar)])) AS JSON) AS patient_info
    FROM ({followup_table})
    {filters}
    GROUP BY patient_id 
    """.format(followup_table=followup_table, project_name=project_name, filters=filters)
    return followup_dictionary



