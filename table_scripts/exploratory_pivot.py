from connections.AWSClient import AWSWorker
import time


def apparitions_to_data(project):
    aws = AWSWorker("athena")
    query = """
    CREATE VIEW v_exploratory_{project} AS
    SELECT patient_id, map_agg(vars_type_id, 1) AS patient_info
    FROM (
        SELECT patient_id, IF(study_vars_type='term_id', concat('t_',term_id), concat('p_',param)) AS vars_type_id
        FROM "default"."exploratory_{project}"
        )
    GROUP BY patient_id
    """.format(
        project=project
    )
    response = aws.athena_queries(query)

apparitions_to_data("senefro")